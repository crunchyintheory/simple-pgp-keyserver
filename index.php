<?php

require_once __DIR__ . '/_auto.php';
ob_start();
?>
<!DOCTYPE html>
<html>
<head>
<base href="/ks/">
<style type="text/css">
pre {
    display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 4px;
}
.uid {
    color: green;
    text-decoration: underline;
}
.fingerprint {
    color: #337ab7;
}
</style>
</head>
<body>
<?php

function keyinfo($key) {
    echo '<pre>';
    printf("<strong>pub</strong> <a href=\"%s/info\" class=\"fingerprint\">%s</a> %s\n\n", $key['subkeys'][0]['fingerprint'], $key['subkeys'][0]['fingerprint'], date('Y-m-d\TH:i:sO', $key['subkeys'][0]['timestamp']));
    foreach($key['uids'] as $uid) {
        printf("<strong>uid</strong> <span class=\"uid\">%s (%s) &lt;%s&gt;</span>\n", $uid['name'], $uid['comment'], $uid['email']);
    }
    echo "\n";
    foreach(array_slice($key['subkeys'], 1) as $subkey) {
        printf("sub <a href=\"%s/info\" class=\"fingerprint\">%s</a> %s\n", $subkey['fingerprint'], $subkey['fingerprint'], date("Y-m-d\TH:i:sO", $subkey['timestamp']));
    }
    echo '</pre>';
}

if($_GET['keyid'] == '') {
    $keys = array();
    foreach(gnupg_keyinfo($res, '') as $key) {
        if($key['is_secret']) continue;
        ob_start();
        keyinfo($key);
        $keys[] = ob_get_clean();
    }
    printf('%d key%s found.<br/><br/>', count($keys), (count($keys) == 1) ? '' : 's');
    echo implode('<hr/><br/>', $keys);
} else {
    if($_GET['action'] == '' || $_GET['action'] == 'get' || $_GET['action'] == 'export') {
        ob_end_clean();
        header('Content-type: text/plain');
        gnupg_setarmor($res, 1);
        echo gnupg_export($res, $_GET['keyid']);
        exit;
    }
    else if($_GET['action'] == 'info' && $_GET['keyid'] != '') {
        if($key = gnupg_keyinfo($res, $_GET['keyid'])) {
            if(!count($key)) {
                echo 'Key not found.';
                exit;
            }
            keyinfo($key[0]);
            printf('<a href="%s"><button>Export</button></a>', $_GET['keyid']);
        }
    }
    else exit;
}

?>
</body>
</html>
<?php
echo ob_get_clean();