<?php

require_once __DIR__ . '/_auto.php';

if(isset($_GET['keyid']) && preg_match('/^0?x?([0-9a-fA-F]*)$/', $_GET['keyid'])) {
    $file = (__DIR__ . '/.keys/' . $_GET['keyid'] . '.asc');
    if(file_exists($file)) {
        gnupg_import($res, file_get_contents($file));
        exit;
    }
}

http_response_code(404);
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL <?=$_SERVER['REQUEST_URI']?> was not found on this server.</p>
<hr>
<address>Apache/2.4.10 (Debian) Server at <?=$_SERVER['SERVER_NAME']?> Port 443</address>
</body></html>